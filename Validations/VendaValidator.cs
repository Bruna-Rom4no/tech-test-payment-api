using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FluentValidation;
using tech_test_payment_api.Models;

namespace tech_test_payment_api.Validations
{
    /// <summary>
    /// Classe responsável por validar os atributor da classe Venda
    /// </summary>
    public class VendaValidator : AbstractValidator<Venda>
    {
        /// <summary>
        /// Responsável pelas especfificações de validação
        /// </summary>
        public VendaValidator()
        {
            RuleFor(v => v.Data)
                .NotNull().WithMessage("O campo não pode ser nulo")
                .NotEmpty().WithMessage("O campo não pode ser vazio");
            
            RuleFor(v => v.ItensVendidos)
                .NotNull().WithMessage("O campo não pode ser nulo")
                .NotEmpty().WithMessage("O campo não pode ser vazio")
                .MinimumLength(3).WithMessage("O campo não atinge o valor mínimo de caracteres")
                .MaximumLength(250).WithMessage("O campo ultrapassa o valor máximo de caracteres");
        }
    }
}