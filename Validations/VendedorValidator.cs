using FluentValidation;
using tech_test_payment_api.Models;

namespace tech_test_payment_api.Validations
{
    /// <summary>
    /// Classe responsável por validar os atributor da classe Vendedor
    /// </summary>
    public class VendedorValidator : AbstractValidator<Vendedor>
    {
        /// <summary>
        /// Responsável pelas especfificações de validação
        /// </summary>
        public VendedorValidator()
        {
            
            RuleFor(x => x.Nome)
                .NotNull().WithMessage("O campo não pode ser nulo")
                .NotEmpty().WithMessage("O campo não pode ser vazio")
                .MinimumLength(3).WithMessage("O campo não atingiu o valor mínimo")
                .MaximumLength(250).WithMessage("O campo ultrapassou o valor máximo.");

            RuleFor(x => x.Telefone)
                .NotNull().WithMessage("O campo não pode ser nulo")
                .NotEmpty().WithMessage("O campo não pode ser vazio")
                .Matches(@"(\(?\d{2}\)?)(\d{4,5}\-\d{4})$").WithMessage("O telefone não é válido.");

            RuleFor(x => x.Email)
                .NotNull().WithMessage("O campo não pode ser nulo")
                .NotEmpty().WithMessage("O campo não pode ser vazio")
                .EmailAddress().WithMessage("O endereço de e-mail não é válido.");

            RuleFor(x => ValidarCPF.Validar(x.Cpf))
                .Equal(true).WithMessage("O documento não é válido.");
        }
    }
}