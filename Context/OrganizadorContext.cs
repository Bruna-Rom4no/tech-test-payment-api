using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using tech_test_payment_api.Models;

namespace tech_test_payment_api.Context
{
    /// <summary>
    /// Classe primária responsável por interagir com o banco de dados.
    /// </summary>
    public class OrganizadorContext : DbContext
    {
        public OrganizadorContext(DbContextOptions<OrganizadorContext> options) : base(options)
        {

        }
        /// <summary>
        /// Resposável pelo mapeamento da tabela de Vendas no banco de dados e operações CRUD.
        /// </summary>
        /// <value></value>
        public DbSet<Venda> Vendas { get; set; }
        /// <summary>
        /// Responsável pelo mapeamento da tabela de Vendedores no banco de dados e operações CRUD.
        /// </summary>
        /// <value></value>
        public DbSet<Vendedor> Vendedores { get; set; }
    }
}