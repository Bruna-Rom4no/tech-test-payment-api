using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion.Internal;

namespace tech_test_payment_api.Models
{
    /// <summary>
    /// Classe venda
    /// </summary>
    public class Venda
    {
        /// <summary>
        /// Identificador da venda
        /// </summary>
        /// <value></value>
        public int Id { get; set; }
        /// <summary>
        /// Data de criação da venda
        /// </summary>
        /// <value></value>
        public DateTime Data { get; set; }
        /// <summary>
        /// Data de Alteração da venda
        /// </summary>
        /// <value></value>
        public DateTime DataAlteracao {get;set;}
        /// <summary>
        /// Itens da venda
        /// </summary>
        /// <value></value>
        public string ItensVendidos { get; set; }
        /// <summary>
        /// Status da venda
        /// AguardandoPagamento,PagamentoAprovado,EnviadoParaTransportadora,Entregue,Cancelada
        /// </summary>
        /// <value></value>
        public EnumStatusVenda Status { get; set; }
        /// <summary>
        /// Identificador do vendedor resposavel pela venda
        /// </summary>
        /// <value></value>
        public int IdVendedor {get;set;}
    }

    /// <summary>
    /// Classe para alteração da venda
    /// </summary>
    public class VendaAlterarStatus
    {
        /// <summary>
        /// Novo status da venda
        /// </summary>
        /// <value></value>
        public EnumStatusVenda Status { get; set; }
    }

    /// <summary>
    /// Classe para retorno de consulta da venda
    /// </summary>
    public class VendaViewModel
    {
        /// <summary>
        /// Identificador da venda
        /// </summary>
        /// <value></value>
        public int Id { get; set; }
        /// <summary>
        /// Data de criação da venda
        /// </summary>
        /// <value></value>
        public DateTime Data { get; set; }
        /// <summary>
        /// Itens da venda
        /// </summary>
        /// <value></value>
        public string ItensVendidos { get; set; }
        /// <summary>
        /// Status da venda
        /// </summary>
        /// <value></value>
        public EnumStatusVenda Status { get; set; }
        /// <summary>
        /// Dados do vendedor responsavel pela venda
        /// </summary>
        /// <value></value>
        public virtual Vendedor Vendedor { get; set; }
    }
}