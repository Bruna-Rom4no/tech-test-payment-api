namespace tech_test_payment_api.Models
{
    /// <summary>
    /// Classe responsável pelos status disponíveis para as vendas
    /// </summary>
    public enum EnumStatusVenda
    {
        AguardandoPagamento,
        PagamentoAprovado,
        EnviadoParaTransportadora,
        Entregue,
        Cancelada
    }
}