using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace tech_test_payment_api.Models
{
    /// <summary>
    /// Classe Vendedor
    /// </summary>
    public class Vendedor
    {
        /// <summary>
        /// Identificador do vendedor
        /// </summary>
        /// <value></value>
        public int Id { get; set; }
        /// <summary>
        /// Nome do vendedor
        /// </summary>
        /// <value></value>
        public string Nome { get; set; }
        /// <summary>
        /// Telefone de contato do vendedor
        /// </summary>
        /// <value></value>
        public string Telefone { get; set; }
        /// <summary>
        /// E-mail do vendedor
        /// </summary>
        /// <value></value>
        public string Email { get; set; }
        /// <summary>
        /// Cadastro de Pessoa Física do vendedor
        /// </summary>
        /// <value></value>
        public string Cpf { get; set; }
        /// <summary>
        /// Lista de vendas atrelada a cada vendedor
        /// </summary>
        /// <value></value>
        [JsonIgnore]
        public virtual List<Venda> Vendas {get;set;}
    }
}