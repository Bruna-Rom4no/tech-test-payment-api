using Microsoft.AspNetCore.Mvc;
using tech_test_payment_api.Context;
using tech_test_payment_api.Models;
using DocumentValidator;

namespace tech_test_payment_api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class VendedorController : ControllerBase
    {
        private readonly OrganizadorContext _context;
        public VendedorController(OrganizadorContext context)
        {
            _context = context;
        }

        /// <summary>
        /// Metodo para cadastrar um vendedor
        /// </summary>
        /// <returns></returns>
        /// <response code="200">Retorna os dados do vendedor</response>
        /// <response code="404">Reveja os dados inseridos</response>
        [HttpPost("v1/Cadastrar")]
        public IActionResult Cadastrar(Vendedor vendedor)
        {
            var valido = CpfValidation.Validate(vendedor.Cpf);
            _context.Vendedores.Add(vendedor);
            _context.SaveChanges();

            return Ok(vendedor);
        }

        /// <summary>
        /// Atualiza dados do vendedor e retorna
        /// </summary>
        /// <param name="id">Identificador do vendedor</param>
        /// <returns></returns>
        /// <response code="200">Retorna os dados do vendedor atualizados</response>
        /// <response code="404">Não existe vendedor para o identificador inserido.</response>
        [HttpPut("v1/{id}")]
        public IActionResult Atualizar(int id, Vendedor vendedor)
        {
            var vendedorBanco = _context.Vendedores.FirstOrDefault(x => x.Id == id);

            if(vendedorBanco == null)
                return NotFound();
            
            vendedorBanco.Nome = vendedor.Nome;
            vendedorBanco.Cpf = vendedor.Cpf;
            vendedorBanco.Telefone = vendedor.Telefone;
            vendedorBanco.Email = vendedor.Email;
            CpfValidation.Validate(vendedorBanco.Cpf);

            _context.Vendedores.Update(vendedorBanco);
            _context.SaveChanges();

            return Ok(vendedorBanco);
        }

        /// <summary>
        /// Deleta o vendedor pelo identificador
        /// </summary>
        /// <param name="id">Identificador do vendedor</param>
        /// <returns></returns>
        /// <response code="200">Deleta o vendedor</response>
        /// <response code="404">Não existe vendedor para o identificador inserido.</response>
        [HttpDelete("v1/{id}")]
        public IActionResult Deletar(int id)
        {
            var vendedorBanco = _context.Vendedores.Find(id);

            if(vendedorBanco == null)
                return NotFound();
            
            _context.Vendedores.Remove(vendedorBanco);
            _context.SaveChanges();

            return NoContent();
        }

        /// <summary>
        /// Retorna todos os vendedores cadastrados
        /// </summary>
        /// <returns></returns>
        /// <response code="200">Retorna os dados de cada vendedor cadastrado</response>
        /// <response code="404">Não existem vendedores cadastrados.</response>
        [HttpGet("v1/ObterTodos")]
        public IActionResult ObterTodos()
        {
            var vendedores = _context.Vendedores.ToList();

            if(vendedores == null)
                return NotFound();

            return Ok(vendedores);
        }

        /// <summary>
        /// Retorna o vendedor pelo identificador
        /// </summary>
        /// <param name="id">Identificador do vendedor</param>
        /// <returns></returns>
        /// <response code="200">Retorna os dados do vendedor</response>
        /// <response code="404">Não existe vendedor para o identificador inserido.</response>
        [HttpGet("v1/{id}")]
        public IActionResult ObterPorId(int id)
        {
            var vendedor = _context.Vendedores.Find(id);

            if(vendedor == null)
                return NotFound();

            return Ok(vendedor);
        }

        /// <summary>
        /// Retorna o vendedor pelo nome
        /// </summary>
        /// <param name="nome">Nome do vendedor</param>
        /// <returns></returns>
        /// <response code="200">Retorna os dados do vendedor</response>
        /// <response code="404">Não existe vendedor com o nome inserido.</response>
        [HttpGet("v1/ObterPorNome")]
        public IActionResult ObterPorNome(string nome)
        {
            var vendedor = _context.Vendedores.Where(x => x.Nome.Contains(nome));

            return Ok(vendedor);
        }
    }
}