using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using tech_test_payment_api.Models;
using tech_test_payment_api.Context;
using Microsoft.EntityFrameworkCore;

namespace tech_test_payment_api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class VendaController : ControllerBase
    {
        private readonly OrganizadorContext _context;
        public VendaController(OrganizadorContext context)
        {
            _context = context;
        }

        /// <summary>
        /// Metodo para criar uma venda
        /// </summary>
        /// <returns>Efetua uma venda e retorna os dados da mesma</returns>
        /// <response code="200">Retorna os dados da venda</response>
        /// <response code="400">A data da venda não pode ser vazia</response>
        [HttpPost("v2/EfetuarVenda")]
        public IActionResult EfetuarVenday(Venda venda)
        {
            if(venda.Data == DateTime.MinValue)
                return BadRequest(new {Erro = "A data da venda não pode ser vazia"});
            
            if(string.IsNullOrWhiteSpace(venda.ItensVendidos))
                return BadRequest(new {Erro = "É necessário ao menos 1 item no pedido para efetuar a venda."});

            if(venda.IdVendedor == null)
                return BadRequest(new {Erro = "Este vendedor não existe."});

            _context.Vendas.Add(venda);
            _context.SaveChanges();

            return RedirectToAction(nameof(ObterPorId), new { id = venda.Id });
        }

        /// <summary>
        /// Retorna a venda pelo identificador
        /// </summary>
        /// <param name="id">Identificador da venda</param>
        /// <returns></returns>
        /// <response code="200">Retorna os dados da venda</response>
        /// <response code="404">Não existe venda para o identificador inserido.</response>
        [HttpGet("{id}")]
        public IActionResult ObterPorId(int id)
        {
            var vendaBanco = _context.Vendas.FirstOrDefault(x => x.Id == id);

            if(vendaBanco == null)
                return NotFound("Não existe venda para o identificador inserido.");

            var viewModel = new VendaViewModel
            {
                Id = vendaBanco.Id,
                Data = vendaBanco.Data,
                ItensVendidos = vendaBanco.ItensVendidos,
                Status = vendaBanco.Status,
                Vendedor = _context.Vendedores.FirstOrDefault(x => x.Id == vendaBanco.IdVendedor)
            };

            return Ok(viewModel);
        }

        /// <summary>
        /// Retorna todas as vendas efetuadas e os dados do vendedor
        /// </summary>
        /// <returns></returns>
        /// <response code="200">Retorna todas as vendas</response>
        /// <response code="404">Não foi efetuada nenhuma venda</response>
        [HttpGet("ObterTodos")]
        public IActionResult ObterTodos()
        {
            var vendas = _context.Vendas.Select(
                x => new VendaViewModel
                {
                    Id = x.Id,
                    Data = x.Data,
                    Status = x.Status,
                    ItensVendidos = x.ItensVendidos,
                    Vendedor = _context.Vendedores.FirstOrDefault(v => v.Id == x.IdVendedor)
                }
            ).ToList();

            if(vendas == null || vendas.Count() == 0)
                return NotFound();
            
            return Ok(vendas);
        }

        /// <summary>
        /// Retorna a venda pela data em que ela foi efetuada
        /// </summary>
        /// <param name="data">Data da venda</param>
        /// <returns></returns>
        /// <response code="200">Retorna os dados da venda</response>
        /// <response code="404">Não existe venda para a data inserida.</response>
        [HttpGet("ObterPorData")]
        public IActionResult ObterPorData(DateTime data)
        {
            var venda = _context.Vendas.Where(x => x.Data.Date == data.Date);
            return Ok(venda);
        }

        /// <summary>
        /// Retorna a venda pelo status
        /// </summary>
        /// <param name="status">status da venda</param>
        /// <returns></returns>
        /// <response code="200">Retorna os dados da venda</response>
        /// <response code="404">Não existe venda com o status inserido.</response>
        [HttpGet("ObterPorStatus")]
        public IActionResult ObterPorStatus(EnumStatusVenda status)
        {
            var venda = _context.Vendas.Where(x => x.Status == status);
            return Ok(venda);
        }

        /// <summary>
        /// Deleta a venda pelo identificador
        /// </summary>
        /// <param name="id">Identificador da venda</param>
        /// <returns></returns>
        /// <response code="200">Deleta os dados da venda</response>
        /// <response code="404">Não existe venda para o identificador inserido.</response>
        [HttpDelete("{id}")]
        public IActionResult Deletar(int id)
        {
            var vendaBanco = _context.Vendas.Find(id);

            if(vendaBanco == null)
                return NotFound();
            
            _context.Vendas.Remove(vendaBanco);
            _context.SaveChanges();

            return NoContent();
        }

        /// <summary>
        /// Retorna a venda pelo identificador
        /// </summary>
        /// <param name="id">Identificador da venda</param>
        /// <param name="statusVenda">Recebe o novo valor a ser passado</param>
        /// <returns></returns>
        /// <response code="200">Retorna os dados da venda com status atualizado</response>
        /// <response code="404">Não existe venda para o identificador inserido.</response>
        [HttpPut("{id}")]
        public IActionResult Atualizar(int id, VendaAlterarStatus statusVenda)
        {
            var vendaBanco = _context.Vendas.Find(id);

            if(vendaBanco == null)
                return NotFound();

            if (statusVenda.Status == EnumStatusVenda.Cancelada && vendaBanco.Status != EnumStatusVenda.AguardandoPagamento && vendaBanco.Status != EnumStatusVenda.PagamentoAprovado)
                return BadRequest("Não foi possível cancelar a venda");
            
            if (statusVenda.Status == EnumStatusVenda.PagamentoAprovado && vendaBanco.Status != EnumStatusVenda.AguardandoPagamento)
                return BadRequest("Essa venda já está com pagamento aprovado");

            if (statusVenda.Status == EnumStatusVenda.AguardandoPagamento && vendaBanco.Status == EnumStatusVenda.PagamentoAprovado)
                return BadRequest("Essa venda já está com pagamento aprovado");

            if (statusVenda.Status == EnumStatusVenda.EnviadoParaTransportadora && vendaBanco.Status != EnumStatusVenda.PagamentoAprovado)
                return BadRequest("Essa venda ainda não está com pagamento aprovado");

            if (statusVenda.Status == EnumStatusVenda.Entregue && vendaBanco.Status != EnumStatusVenda.EnviadoParaTransportadora)
                return BadRequest("Não enviada para a transportadora");
            
            if (statusVenda.Status == EnumStatusVenda.AguardandoPagamento && vendaBanco.Status == EnumStatusVenda.Entregue)
                return BadRequest("Venda já finalizada");

            vendaBanco.Status = statusVenda.Status;
            vendaBanco.DataAlteracao = DateTime.Now;

            _context.Vendas.Update(vendaBanco);
            _context.SaveChanges();

            return Ok(vendaBanco);
        }
    }
}