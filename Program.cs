using System.Text.Json.Serialization;
using Microsoft.EntityFrameworkCore;
using tech_test_payment_api.Context;
using FluentValidation;
using FluentValidation.AspNetCore;
using tech_test_payment_api.Validations;
using tech_test_payment_api.Models;
using FluentValidation.Results;
using System.Reflection;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.AddControllers()
    .AddFluentValidation(c => c.RegisterValidatorsFromAssembly(Assembly.GetExecutingAssembly()));

builder.Services.AddDbContext<OrganizadorContext>(opt => opt.UseInMemoryDatabase("Database"));
builder.Services.AddScoped<OrganizadorContext, OrganizadorContext>();

builder.Services.AddControllers().AddJsonOptions(options => 
    // Mostrar a descrição do enum em vez do valor númerico
    options.JsonSerializerOptions.Converters.Add(new JsonStringEnumConverter()) );
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen(s => {
    var basePath = AppContext.BaseDirectory;
    var assemblyName = System.Reflection.Assembly.GetEntryAssembly().GetName().Name;
    var fileName = System.IO.Path.GetFileName(assemblyName + ".xml");
    s.IncludeXmlComments(System.IO.Path.Combine(basePath, fileName));
}); 

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();
